<?php

$tasks = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];



if(isset($_GET['index'])){ //will check if it can GET any data passed via URL(specifically in this case, data labeled "index")

	$i = $_GET['index']; //containes the value of "index"

	echo "The retrieved task from GET is $tasks[$i]."; //show the task with the same index chosen from the below select

}


if(isset($_POST['index'])){ //will receieve the data submitted via POST

	$i = $_POST['index']; //containes the value of "index"

	echo "The retrieved task from _POST is $tasks[$i]."; //show the task with the same index chosen from the below select

}

// When submitting form data, the difference between GET and POST is that GET sends data via the URL (which will be visible to the user), and POST does not (and is therefore more secure)

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S5: Client-Server Communication (GET and POST)</title>
</head>
<body>
	<h1>Task Index from GET</h1>
	<form method="GET">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>
	</form>


	<h1>Task Index from POST</h1>
	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>

</body>
</html>


















